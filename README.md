# yfcmf-tp6插件

#### 介绍
由于yfcmf-tp6没有官方上传插件的仓库，所以临时使用这个仓库




#### 安装教程

直接下载项目，将zip包，在项目中点击离线安装，选择zip包，即可安装插件


#### 参与贡献
沐雨轩


#### 包
mcloudstorage-1.0.1-tp6.zip     云存储插件，官方的不支持腾讯云，这个支持

mbulider-1.0.5-tp6.zip     快速构建器插件，几行php代码即可创建表单和表格

dataui-1.0.9-tp6.zip     数据库ui可视化插件


dataui-1.0.8.zip       数据库可视化插件（fastadmin版）

mbulider-1.0.4.zip    快速构建器插件，几行php代码即可创建表单和表格（fastadmin版）



